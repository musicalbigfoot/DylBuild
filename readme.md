# DylBuild 1.0

### How to use DylBuild

These instructions are assuming you do not have an existing package.json file. You should expect to have to modify some files when copying to the project directory if anything needs to be overwritten.

* Copy all files into project directory
* Set url of dev site in **gulpfile.js**
* Update project details in **package.json**
* Run `npm install` and `typings install` in the project directory 
	* Note: you'll need node and typescript installed for these to work
* Finally, run `gulp` in the project directory to start the build system.